package ua.danit.application.model;

public class Tickets {
    private Long id;
    private Long customer_id;
    private String from;
    private String to;


    public Tickets(){
    }

    public Tickets(Long id, long customer_id, String from, String to){
        this.id = id;
        this.customer_id = customer_id;
        this.from = from;
        this.to = to;
    }

    public Long getId() { return id; }

    public long getCustomerId() {
        return customer_id;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }
}
