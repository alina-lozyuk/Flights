package ua.danit.application.model;

public class Airport {
  /*

  Airport ID	Unique OpenFlights identifier for this airport.
Name	Name of airport. May or may not contain the City name.
City	Main city served by airport. May be spelled differently from Name.
Country	Country or territory where airport is located. See countries.dat to cross-reference to ISO 3166-1 codes.
IATA	3-letter IATA code. Null if not assigned/unknown.
ICAO	4-letter ICAO code.
Null if not assigned.
Latitude	Decimal degrees, usually to six significant digits. Negative is South, positive is North.
Longitude	Decimal degrees, usually to six significant digits. Negative is West, positive is East.
Altitude	In feet.
Timezone	Hours offset from UTC. Fractional hours are expressed as decimals, eg. India is 5.5.
DST	Daylight savings time. One of E (Europe), A (US/Canada), S (South America), O (Australia), Z (New Zealand), N (None) or U (Unknown). See also: Help: Time
Tz database time zone	Timezone in "tz" (Olson) format, eg. "America/Los_Angeles".
Type	Type of the airport. Value "airport" for air terminals, "station" for train stations, "port" for ferry terminals and "unknown" if not known. In airports.csv, only type=airport is included.
Source	Source of this data. "OurAirports" for data sourced from OurAirports, "Legacy" for old data not matched to OurAirports (mostly DAFIF), "User" for unverified user contributions. In airports.csv, only source=OurAirports is included.
The data is UTF-8 (Unicode) encoded.

Note: Rules for daylight savings time change from year to year and from country to country. The current data is an approximation for 2009, built on a country level. Most airports in DST-less regions in countries that generally observe DST (eg. AL, HI in the USA, NT, QL in Australia, parts of Canada) are marked incorrectly.

Sample entries
507,"London Heathrow Airport","London","United Kingdom","LHR","EGLL",51.4706,-0.461941,83,0,"E","Europe/London","airport","OurAirports"
26,"Kugaaruk Airport","Pelly Bay","Canada","YBB","CYBB",68.534401,-89.808098,56,-7,"A","America/Edmonton","airport","OurAirports"
3127,"Pokhara Airport","Pokhara","Nepal","PKR","VNPK",28.200899124145508,83.98210144042969,2712,5.75,"N","Asia/Katmandu","airport","OurAirports"
8810,"Hamburg Hbf","Hamburg","Germany","ZMB",\N,53.552776,10.006683,30,1,"E","Europe/Berlin","station","User"
   */

  public Airport() {
  }

    public Airport(long airportID, String name, String city, String country, String iATA, String iCAO, long latitude, long longitude, long altitude, long timezone, long daylightSavingsTime, String databaseTimeZone, String type, String callsign, String source) {
        this.airportID = airportID;
        this.name = name;
        this.city = city;
        this.country = country;
        this.iATA = iATA;
        this.iCAO = iCAO;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.timezone = timezone;
        this.daylightSavingsTime = daylightSavingsTime;
        this.databaseTimeZone = databaseTimeZone;
        this.type = type;
        this.callsign = callsign;
        this.source = source;
    }

    public long getAirportID() {
        return airportID;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getiATA() {
        return iATA;
    }

    public String getiCAO() {
        return iCAO;
    }

    public long getLatitude() {
        return latitude;
    }

    public long getLongitude() {
        return longitude;
    }

    public long getAltitude() {
        return altitude;
    }

    public long getTimezone() {
        return timezone;
    }

    public long getDaylightSavingsTime() {
        return daylightSavingsTime;
    }

    public String getDatabaseTimeZone() {
        return databaseTimeZone;
    }

    public String getType() {
        return type;
    }

    public String getCallsign() {
        return callsign;
    }

    public String getSource() {
        return source;
    }

    private long airportID;

    private String name;

    private String city;

    private String country;

    private String iATA;

    private String iCAO;

    private long latitude;

    private long longitude;

    private long altitude;

    private long timezone;

    private long daylightSavingsTime;

    private String databaseTimeZone;

    private String type;

    private String callsign;

    private String source;
}
